from django.contrib import admin

from .models import EmailList, Volunteer

admin.site.register(EmailList)
admin.site.register(Volunteer)
