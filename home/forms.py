from django import forms
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

from .models import EmailList, Volunteer

class EmailForm(forms.ModelForm):
    class Meta:
        model = EmailList
        fields = ('first_name', 'email')

class VolunteerForm(forms.ModelForm):
    phone_number = PhoneNumberField(widget=PhoneNumberPrefixWidget)
    description = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Volunteer
        fields = ('first_name', 'last_name', 'email', 'phone_number', 'description')