from django.urls import path

from . import views

app_name='home'
urlpatterns = [
    path('', views.index, name='home'),
    path('what/', views.what, name='what'),
    path('how/', views.how, name='how'),
    path('email/', views.email_list, name='email'),
    path('projects/', views.projects, name='projects'),
    path('volunteer/', views.volunteer, name='volunteer'),
    path('privacy/', views.privacy, name='privacy')
]

