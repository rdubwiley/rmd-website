from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from .forms import EmailForm, VolunteerForm
from .models import EmailList, Volunteer


def index(request):
    return render(request, 'home/index.html')


def what(request):
    return render(request, 'home/what.html')


def how(request):
    return render(request, 'home/how.html')


def projects(request):
    return render(request, 'home/projects.html')

def privacy(request):
    return render(request, 'home/privacy.html')

def email_list(request):
    if request.method == 'POST':
        email_list_form = EmailForm(data=request.POST)
        if email_list_form.is_valid():
            email_list_form.save()
            msg_html = render_to_string(
                'email/email_list_signup.html',
                {'first_name': email_list_form.cleaned_data['first_name']}
            )
            text_content = strip_tags(msg_html)
            msg = EmailMultiAlternatives(
                'Reform MI Dems Email List Signup',
                text_content,
                from_email=settings.EMAIL_HOST_USER,
                to=[email_list_form.cleaned_data['email']],
                bcc=[settings.EMAIL_BCC]
            )
            msg.attach_alternative(msg_html, "text/html")
            msg.send()
            messages.success(request, 'You have been added to the email list')
    else:
        email_list_form = EmailForm()
    return render(request, 'home/email_list.html', {'email_list_form': email_list_form})


def volunteer(request):
    if request.method == 'POST':
        volunteer_form = VolunteerForm(data=request.POST)
        if volunteer_form.is_valid():
            volunteer_form.save()
            msg_html = render_to_string(
                'email/volunteer_signup.html',
                {'first_name':volunter_form.cleaned_data['first_name']}
            )
            text_content = strip_tags(msg_html)
            msg = EmailMultiAlternatives(
                'Reform MI Dems Volunteer Signup',
                text_content,
                from_email=settings.EMAIL_HOST_USER,
                to=[email_list_form.cleaned_data['email']],
                bcc=[settings.EMAIL_BCC]
            )
            msg.attach_alternative(msg_html, "text/html")
            msg.send()
            messages.success(request, 'Thank you for volunteering, we will reach out to you shortly')
    else:
        volunteer_form = VolunteerForm()
    return render(request, 'home/volunteer.html', {'volunteer_form': volunteer_form})
