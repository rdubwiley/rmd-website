from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

class EmailList(models.Model):
    first_name = models.CharField(max_length=50)
    email = models.EmailField()

class Volunteer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    phone_number = PhoneNumberField(blank=True)
    description = models.CharField(max_length=1000)
